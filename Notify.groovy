def rocketchat_send( rocketurl, channel, link_message, title, message, color ){
    def top_message = "${env.JOB_NAME.replace('/',' > ')} # ${env.JOB_NUMBER} - ${env.JOB_URL}";

    sh('curl -H "X-Auth-Token: TOKEN" -H "X-User-Id: TOKEN" -H "Content-Type: application/json" "https://${rocketurl}/api/v1/chat.postMessage" '
        +"-d '{\"text\":\"${top_message}\", \"avatar\":\"https://wiki.jenkins.io/download/attachments/2916393/headshot.png\",\"alias\":\"Jenkins\",\"emojis\":\":rocket:\",\"channel\":\"#${channel}\",\"attachments\":[{\"color\":\"${color}\",\"title\":\"${title}\",\"text\":\"${message}\",\"title_link\":\"${title_link}\"}]}'"
    )
}

def echo_world( ){
    sh "echo 'Hello W0rld !'"
}

return this
